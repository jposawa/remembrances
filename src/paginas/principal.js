import React, {Component} from 'react';

import Menu from "../componentes/menu.js";
import Inicio from "../componentes/inicio.js";
import Msg1 from "../componentes/mensagem.js";
import Msg2 from "../componentes/presente.js";

import "./styles.css";

const uuid = {
    v1: require('uuid/v1'),
    v4: require('uuid/v4'),
}

const prefixoLS = "rem@";

class Principal extends Component{
    constructor(props){
        super(props);
             
        this.state={
            menuAberto:false,
            pagina:"INICIO",
            flags:{
                flag1:false,
                flag2:false,
                flag3:false,
            }
        };
    }

    componentDidMount()
    {
        this.ChecaFlags();
    }

    componentDidUpdate(prevProps, prevState)
    {
        const {props, state} = this;
        const prevGeral = {...prevProps.geral};
        const {geral} = props;

        // if(prevState.pagina !== state.pagina)
        // {
        /* switch(state.pagina)
        {
            case "FICHAS":
                this.PuxaFichas();
            break;
        } */
        // }
    }

    FormataTempo = (tempo, padrao) =>{
        tempo = parseFloat(tempo);
        padrao = parseFloat(padrao);

        if(isNaN(padrao))
        {
            padrao = 1;
        }

        if(isNaN(tempo))
        {
            tempo = padrao;
        }

        tempo *= 1000;

        return tempo;
    }

    ApareceMensagem = (tempo,blocoMsg) =>{
        const campo = document.getElementById(blocoMsg);

        // console.log(campo);
        tempo = this.FormataTempo(tempo);

        if(blocoMsg === "todas" || blocoMsg === undefined || blocoMsg === null || blocoMsg === "")
        {
            const campos = document.getElementsByName("msg");

            if(campos.length > 0)
            {
                let i = 0;
                let intervalo = setInterval(() => {
                    if(i < campos.length){
                        campos[i++].classList.remove("msgEscondida");
                    }
                    else
                    {
                        clearInterval(intervalo);
                    }
                }, tempo);
            }
        }
        else if(campo !== null && campo !== undefined)
        {
            setTimeout(() => {
                campo.classList.remove("msgEscondida");
            }, tempo);
        }
    }

    AlternaMenu = (evento) =>{
        const slotMenu = document.getElementById("slotMenu");
        let campo

        if(evento !== null && evento !== undefined)
        {
            campo = evento.target;
        }
        else
        {
            campo = document.getElementById("botaoMenu");
        }

        // console.log(campo);

        campo.classList.toggle("btnAberto");
        slotMenu.classList.toggle("menuAberto");
    }

    AtivaFlag = (flag) =>{
        const {flags} = this.state;

        if(flag !== null && flag !== undefined)
        {
            flags[flag] = true;

            localStorage.setItem(prefixoLS + flag,true);

            this.setState({flags:flags});
        }
    }

    ChecaFlags = () =>{
        const {flags} = this.state;

        const chavesFlags = Object.keys(flags);

        chavesFlags.forEach(flag => {
            const resultado = localStorage.getItem(prefixoLS+flag);
            
            if(resultado)
            {
                flags[flag] = true;
            }
        });

        this.setState({flags:flags});
        // console.log(chavesFlags);
    }

    MudaPagina = (evento) =>{
        let nomePagina;

        if(evento !== null && evento !== undefined)
        {
            const campo = evento.target;
            nomePagina = campo.id;
            nomePagina = nomePagina.toUpperCase();
        }
        else
        {
            nomePagina = "INICIO";
        }

        this.AlternaMenu();
        this.setState({pagina:nomePagina});
    }

    

    render()
    {
        const {state, props} = this;

        return(
            <div id="corpoSite">
                <div id="slotCorpo" onClick={this.CliqueTela}>
                    {
                        {
                            'INICIO': <Inicio
                                geral = {this.state}
                                AlternaMenu = {this.AlternaMenu}
                                ApareceMensagem = {this.ApareceMensagem}
                            />,

                            'MSG1':<Msg1
                                geral = {this.state}
                                ApareceMensagem = {this.ApareceMensagem}
                                AtivaFlag = {this.AtivaFlag}
                            />,
                            
                            'MSG2':<Msg2
                                geral = {this.state}
                                ApareceMensagem = {this.ApareceMensagem}
                                AtivaFlag = {this.AtivaFlag}
                            />,
                            /* 'PERFIL': <Perfil
                                usuario = {props.geral.usuario}
                                AtualizaDados = {props.AtualizaDados}
                                AtualizaEmail = {props.AtualizaEmail}
                                AtualizaSenha = {props.AtualizaSenha}
                            />, */
                            
                        }[state.pagina]
                    }
                </div>
                <div id="campoMenu">
                    <Menu
                        geral = {this.state}
                        MudaPagina = {this.MudaPagina}
                        AlternaMenu = {this.AlternaMenu}
                        AtivaFlag = {this.AtivaFlag}
                    />
                </div>
            </div>
        );
    }
}

export default Principal;