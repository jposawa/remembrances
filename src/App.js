import React from 'react';
import logo from './logo.svg';
import './App.css';
// import './componentes/styles.css';
import Principal from './paginas/principal.js';

function App() {
  return (
    <div className="App">
      <Principal/>
    </div>
  );
}

export default App;
