import React, {Component} from 'react';
// import "//cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.1/leaflet.css"

// import Mapa from "../mapa";



export default class Msg1 extends Component{
    constructor(props){
        super(props);
        
        this.state={};
    }

    componentDidMount()
    {
        // console.log("plw");
        const {props} = this;

        props.ApareceMensagem(2);
        props.ApareceMensagem(20,"msg1");
        props.AtivaFlag("flag1");
    }

    componentDidUpdate(prevProps, prevState)
    {
        const {props, state} = this;
        const prevGeral = {...prevProps.geral};
        const {geral} = props;

        // console.log(geral);

        
        // console.log(prevGeral.listaPin[0].length);

        if(prevProps !== null)
        {
            
        }
        else
        {
            
        }
    }

    render()
    {
        const {state, props} = this;
        const {geral} = props;

        return(
            <div id="corpoPagina">
                <h2>Algumas palavras...</h2>
                
                <p className="msgEscondida" name="msg">Você sabe que eu gosto de textão e você também sabe que eu te amo muito.</p>
                <p className="msgEscondida" name="msg">Passei um tempão pensando no que poderia fazer pra você nessa data tão especial e foi difícil, pois o tempo tem sido corrido e também não queria fazer qualquer coisa de qualquer jeito.</p>
                <p className="msgEscondida" name="msg">Enquanto pensava nisso lembrei do primeiro presente de aniversário que eu te dei. E que eu acho que você já deve ter lembrado também caso tenha clicado no botão <i>Lembrança</i> do menu.</p>
                <br/>
                <p className="msgEscondida" name="msg">Nós já passamos por tantas coisas não foi?</p>
                <p className="msgEscondida" name="msg">Tantas pessoas que tentaram e tentam nos separar, nos colocar um contra o outro...</p>
                <p className="msgEscondida" name="msg">Por mais que esses momentos doam, sempre lembro de tudo o que já passamos até agora, de como nosso amor é lindo e como nós somos demais juntos.</p>
                <p className="msgEscondida" name="msg">Hoje é uma renovação do seu ciclo de vida e pode ter certeza que o maior presente é você na vida de todo mundo que te conhece.</p>
                <p className="msgEscondida" name="msg">Você é uma pessoa muito preciosa, talentosa...</p>
                <p className="msgEscondida" name="msg">Magnífica em todos os sentidos...</p>
                <p className="msgEscondida" id="msg1">Acho que algo liberou em seu menu...</p>
            </div>
        );
    }
}
// export default Painel;