import React, {Component} from 'react';
// import "//cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.1/leaflet.css"

// import Mapa from "../mapa";



export default class Msg2 extends Component{
    constructor(props){
        super(props);
        
        this.state={};
    }

    componentDidMount()
    {
        // console.log("plw");
        const {props} = this;

        props.ApareceMensagem(2.5);
    }

    componentDidUpdate(prevProps, prevState)
    {
        const {props, state} = this;
        const prevGeral = {...prevProps.geral};
        const {geral} = props;

        // console.log(geral);

        
        // console.log(prevGeral.listaPin[0].length);

        if(prevProps !== null)
        {
            
        }
        else
        {
            
        }
    }

    render()
    {
        const {state, props} = this;
        const {geral} = props;

        return(
            <div id="corpoPagina">
                <h2>Presente</h2>
                
                <p className="msgEscondida" name="msg">Inicialmente pensei em fazer um jogo, como ano passado.</p>
                <p className="msgEscondida" name="msg">Mas como eu disse, o tempo foi curto e também as ideias não fluíram tanto assim...</p>
                <br/>
                <p className="msgEscondida" name="msg">Portanto, além desse site para relembrar o primeiro presente de aniversário que eu te fiz, também pensei em fazer uma janta especial</p>
                <p className="msgEscondida" name="msg">Não é nada muito chique ou elaborado...</p>
                <p className="msgEscondida" name="msg">Só que você pode ter certeza de que é de coração.</p>
                <br/>
                <p className="msgEscondida" name="msg">Também quero deixar bem claro que, não importam os ataques, ou como as situações no momento estejam difíceis...</p>
                <br/>
                <p className="msgEscondida" name="msg">Sempre estarei aqui por você.</p>
                <p className="msgEscondida" name="msg">Você é o <b>Amor da minha vida</b>!</p>
                <br/>
                <p className="msgEscondida" name="msg"><b>Te amo Dulin!</b></p>
            </div>
        );
    }
}
// export default Painel;