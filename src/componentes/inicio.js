import React, {Component} from 'react';
// import "//cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.1/leaflet.css"

// import "./styles.css";

// import Mapa from "../mapa";


const prefixoLS = "rem@";

export default class Inicio extends Component{
    constructor(props){
        super(props);
        
        this.state={};
    }

    componentDidMount()
    {
        // console.log("plw");
        const {props} = this;

        if(this.PrimeiraVez())
        {
            props.ApareceMensagem(2);
        }
        else
        {
            props.ApareceMensagem(0.1,"msg6");
            props.ApareceMensagem(0.5,"msg7");
            props.ApareceMensagem(1,"slotMenu");
        }
        // props.ApareceMensagem("msg2",4);
    }

    componentDidUpdate(prevProps, prevState)
    {
        const {props, state} = this;
        const prevGeral = {...prevProps.geral};
        const {geral} = props;

        // console.log(geral);

        
        // console.log(prevGeral.listaPin[0].length);

        if(prevProps !== null)
        {
            
        }
        else
        {
            
        }
    }

    PrimeiraVez = () =>{
        const acessou = localStorage.getItem(prefixoLS+"acessou");

        return !acessou;

        // console.log(acessou);
    }

    RespondeInicio = (resposta) =>{
        const {props} = this;

        if(resposta)
        {
            localStorage.setItem(prefixoLS+"acessou",true);

            props.ApareceMensagem(0.1,"msg5");
            props.ApareceMensagem(1,"slotMenu");
        }
        else
        {
            props.ApareceMensagem(0.1,"msg4");
        }

        const campoOp = document.getElementById("opcoes1");

        campoOp.classList.add("msgEscondida","posicaoAbsoluta");
    }

    render()
    {
        const {state, props} = this;
        const {geral} = props;

        return(
            <div id="corpoPagina">
                <h2>Oi Dulin...</h2>
                
                <p id="msg0" name="msg" className="msgEscondida">Você lembra de dois anos atrás?</p>
                <p id="msg1" name="msg" className="msgEscondida">Faça um teste de Inteligência</p>
                <p id="msg2" name="msg" className="msgEscondida">Brincando...</p>
                <p id="msg3" name="msg" className="msgEscondida">Só queria falar algumas coisas para você</p>
                <p id="opcoes1" name="msg" className="msgEscondida">
                    <p>Você está com tempo para prosseguir?</p>
                    <p className="linhaOpcoes">
                        <button type="button" className="btnNegativo" onClick={() => this.RespondeInicio(false)}>Não</button>
                        <button type="button" className="btnPositivo" onClick={() => this.RespondeInicio(true)}>Sim</button>
                    </p>
                </p>
                <p id="msg4" className="msgEscondida">Tudo bem, quando você puder, volte aqui</p>
                <p id="msg5" className="msgEscondida">Para prosseguirmos, clique no menu abaixo...</p>
                <p id="msg6" className="msgEscondida">Seja bem-vinda de volta. Lembre-se do menu abaixo</p>
                <p id="msg7" className="msgEscondida">Te amo</p>
            </div>
        );
    }
}
// export default Painel;